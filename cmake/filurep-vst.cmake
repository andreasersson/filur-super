include(FetchContent)

set(target filurep-vst)

FetchContent_Declare(
  ${target}
  GIT_REPOSITORY https://gitlab.com/andreasersson/filurep-vst.git
  GIT_TAG        master
)

FetchContent_GetProperties(${target})
if(NOT ${target}-vst_POPULATED)
  FetchContent_Populate(${target})

  add_subdirectory(${${target}_SOURCE_DIR} ${${target}_BINARY_DIR})
endif()
