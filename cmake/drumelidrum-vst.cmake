include(FetchContent)

set(target drumelidrum-vst)

FetchContent_Declare(
  ${target}
  GIT_REPOSITORY https://gitlab.com/andreasersson/drumelidrum-vst.git
  GIT_TAG        master
)

FetchContent_GetProperties(${target})
if(NOT ${target}_POPULATED)
  FetchContent_Populate(${target})

  add_subdirectory(${${target}_SOURCE_DIR} ${${target}_BINARY_DIR})
endif()
