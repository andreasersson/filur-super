set(BUILD_TESTS OFF CACHE BOOL "Enable testing.")

if(BUILD_TESTS)
  enable_testing()
  include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/gtest.cmake)
endif(BUILD_TESTS)
