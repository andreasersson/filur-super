Super build repository for
[filur][filur],
[filur-vst][filur-vst],
[filurep][filurep],
[polyfilur][polyfilur] and
[drumelidrum][drumelidrum].

---
## How to build.
### Requirements
- [CMake][CMake] 3.11.0 or later.

### Dependencies
- [Steinberg VST 3 Plug-In SDK][VST3SDK]
- [googletest][googletest]. Only used if BUILD_TESTS is enabled.
- [filur][filur]
- [filur-vst][filur-vst]
- [filurep][filurep],
- [polyfilur][polyfilur]
- [drumelidrum][drumelidrum]

*The dependencies will be automatically cloned and built using the [CMake FetchContent module][FetchContent]*  
*On some Linux distributions you might have to install the dependencies needed by [VSTGUI][VSTGUI].*

Download the source or clone the git repository.

    git clone git@gitlab.com:andreasersson/filur-super.git

### Linux/macOS with Makefiles
*Note that Linux support is still experimental in [VST3SDK][VST3SDK].*

    mkdir build-filur-super && cd build-filur-super
    cmake -DCMAKE_BUILD_TYPE=Release ../filur-super
    cmake --build .
##### CMake 3.13.0 or later.
    cmake -DCMAKE_BUILD_TYPE=Release -S filur-super -B build-filur-super
    cmake --build build-filur-super

### macOS with Xcode
    mkdir build-filur-super && cd build-filur-super
    cmake -GXcode ../filur-super
    cmake --build . --config Release
##### CMake 3.13.0 or later.
    cmake -GXcode -S filur-super -B build-filur-super
    cmake --build build-filur-super --config Release

### Windows with Visual Studio 15 2017
    cmake -G"Visual Studio 15 2017" -A x64 -S filur-super -B build-filur-super
    cmake --build build-filur-super --config Release

## License
[![GPLv3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png "GNU General Public License")](https://www.gnu.org/licenses/gpl.html)

    This program is free software: you can redistribute it and/or modify   
    it under the terms of the GNU General Public License as published by   
    the Free Software Foundation, either version 3 of the License, or   
    (at your option) any later version.   

    This program is distributed in the hope that it will be useful,   
    but WITHOUT ANY WARRANTY; without even the implied warranty of   
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   
    GNU General Public License for more details.

[CMake]: https://cmake.org/
[FetchContent]: https://cmake.org/cmake/help/latest/module/FetchContent.html
[googletest]: https://github.com/abseil/googletest
[VST3SDK]: https://github.com/steinbergmedia/vst3sdk
[VSTGUI]: https://github.com/steinbergmedia/vstgui
[filur]: https://gitlab.com/andreasersson/filur
[filur-vst]: https://gitlab.com/andreasersson/filur-vst
[filurep]: https://gitlab.com/andreasersson/filurep-vst
[polyfilur]: https://gitlab.com/andreasersson/polyfilur-vst
[drumelidrum]: https://gitlab.com/andreasersson/drumelidrum-vst
